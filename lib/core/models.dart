import 'package:flutter/material.dart';

class DnsConfig {
  final String dns1;
  final String dns2;

  DnsConfig(this.dns1, this.dns2);
}

abstract class Model {
  Map<String, dynamic> toJson();
  String toString() => toJson().toString();
}

class VpnConfig extends Model {
  VpnConfig({
    @required this.name,
    this.username,
    this.password,
    @required this.config,
    this.flag
  });

  String name;
  String username;
  String password;
  String config;
  String flag;

  factory VpnConfig.fromJson(Map<String, dynamic> json) => VpnConfig(
        name: json["name"] == null ? null : json["name"],
        username: json["username"] == null ? null : json["username"],
        password: json["password"] == null ? null : json["password"],
        config: json["config"] == null ? null : json["config"],
        flag: json["flag"] == null ? null : json["flag"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "username": username == null ? null : username,
        "password": password == null ? null : password,
        "config": config == null ? null : config,
        "flag": flag == null ? null : flag,
      };
}

class VpnStatus extends Model {
  VpnStatus({
    this.duration,
    this.lastPacketReceive,
    this.byteIn,
    this.byteOut,
  });

  String duration;
  String lastPacketReceive;
  String byteIn;
  String byteOut;

  factory VpnStatus.fromJson(Map<String, dynamic> json) => VpnStatus(
        duration: json["duration"],
        lastPacketReceive: json["last_packet_receive"],
        byteIn: json["byte_in"],
        byteOut: json["byte_out"],
      );

  Map<String, dynamic> toJson() => {
        "duration": duration,
        "last_packet_receive": lastPacketReceive,
        "byte_in": byteIn,
        "byte_out": byteOut,
      };
}