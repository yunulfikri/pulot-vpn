

import 'dart:convert';

import 'package:flutter/services.dart';
import 'models.dart';
import 'package:package_info/package_info.dart';

class PulotVpn {

  // ------
  // ###### Class untuk berkomunikasi ke Java Native
  // ------

  // channel name untuk komunikasi java native
  static final String _eventChannelVpnStage = "com.ruangbawah.pvpn/vpnstage";
  static final String _eventChannelVpnStatus = "com.ruangbawah.pvpn/vpnstatus";
  static final String _methodChannelVpnControl = "com.ruangbawah.pvpn/vpncontrol";

  // ambil data untuk stage VPN dari Native
  static Stream<String> vpnStageSnapshot() => EventChannel(_eventChannelVpnStage).receiveBroadcastStream().cast();

  // ambil data untuk status VPN dari Native
  static Stream<VpnStatus> vpnStatusSnapshot() => 
    EventChannel(_eventChannelVpnStatus).receiveBroadcastStream()
    .map((event) => VpnStatus.fromJson(jsonDecode(event))).cast();
  

  // bagian untuk start VPN
  static Future<void> startVpn(VpnConfig vpnConfig, {DnsConfig dns, List<String> bypassPackages}) async{
    final package = await PackageInfo.fromPlatform();
    if (bypassPackages == null) {
      bypassPackages = [package.packageName];
    } else {
      bypassPackages.add(package.packageName);
    }

    return MethodChannel(_methodChannelVpnControl).invokeMethod(
      "start",
      {
        "config": vpnConfig.config,
        "country": vpnConfig.name,
        "username": vpnConfig.username ?? "",
        "password": vpnConfig.password ?? "",
        "dns1": dns?.dns1,
        "dns2": dns?.dns2,
        "bypass_packages": bypassPackages,
      }
    );
  }

    ///Stop vpn
  static Future<void> stopVpn() => MethodChannel(_methodChannelVpnControl).invokeMethod("stop");

  ///Open VPN Settings
  static Future<void> openKillSwitch() => MethodChannel(_methodChannelVpnControl).invokeMethod("kill_switch");

  ///Trigger native to get stage connection
  static Future<void> refreshStage() => MethodChannel(_methodChannelVpnControl).invokeMethod("refresh");

  ///Get latest stage
  static Future<String> stage() => MethodChannel(_methodChannelVpnControl).invokeMethod("stage");

  ///Check if vpn is connected
  static Future<bool> isConnected() => stage().then((value) => value.toLowerCase() == "connected");

  ///All Stages of connection
  static const String vpnConnected = "connected";
  static const String vpnDisconnected = "disconnected";
  static const String vpnWaitConnection = "wait_connection";
  static const String vpnAuthenticating = "authenticating";
  static const String vpnReconnect = "reconnect";
  static const String vpnNoConnection = "no_connection";
  static const String vpnConnecting = "connecting";
  static const String vpnPrepare = "prepare";
  static const String vpnDenied = "denied";
}