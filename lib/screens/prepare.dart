import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pulotvpn/screens/homepage.dart';
import 'package:pulotvpn/screens/loading.dart';
import 'package:device_info/device_info.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';


class PrepareScreen extends StatefulWidget {
  const PrepareScreen({Key key}) : super(key: key);

  @override
  _PrepareScreenState createState() => _PrepareScreenState();
}

class _PrepareScreenState extends State<PrepareScreen> {
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  CollectionReference configRef = FirebaseFirestore.instance.collection('config');
  FirebaseAuth auth = FirebaseAuth.instance;

  UserCredential userCredential;

  String deviceName ='';
  String identifier= '';
  String serverName= '';
  String serverfile;

  Future<void>_deviceDetails() async{
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        setState(() {
          deviceName = build.model;
          identifier = build.androidId;
        });
        //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        setState(() {
          deviceName = data.name;
          identifier = data.identifierForVendor;
        });//UUID for iOS
      }
    } on PlatformException {
      print('Failed to get platform version');
    }
  }



  Future<void> configFileServer() async {
    userCredential = await FirebaseAuth.instance.signInAnonymously();
    String docid;
    String ovpnfile;
    String server;
    await configRef.where('downloaded', isEqualTo: false)
        .limit(1)
        .get()
        .then((QuerySnapshot snapshot){
            snapshot.docs.forEach((doc) {
              docid = doc.id;
              ovpnfile = doc['filename'];
              server = doc['server'];
            });
    });
    
    await configRef.doc(docid).update({'downloaded': true}).then((value) => print("server updated")).catchError((error) => print("Failed to update server: $error"));

    setState(() {
      serverName = server;
      serverfile = ovpnfile;
    });

  }

  prepare() async{
      await _deviceDetails();
      await configFileServer();
      userCredential = await FirebaseAuth.instance.signInAnonymously();
      // Call the user's CollectionReference to add a new user
      await users.add({
        'deviceName': deviceName,
        'identifier': identifier,
        'uid': userCredential.user.uid,
        'client': serverfile
      }).then((value) => print("User Added")).catchError((error) => print("Failed to add user: $error"));

      SharedPreferences _sharedPreference = await SharedPreferences.getInstance();
      await _sharedPreference.setBool('ready', true);
      await _sharedPreference.setString('serverfile', serverfile);
      // Navigator.pop(context);
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => HomePageScreen())
      );
  }

  @override
  void initState(){
    // TODO: implement initState
    prepare();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return LoadingScreen();
  }
}
