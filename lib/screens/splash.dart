import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pulotvpn/screens/homepage.dart';
import 'package:pulotvpn/screens/onboarding.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future check() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _key = prefs.getString('acceptterms');
    if (_key ==  null) {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => new OnBoardingPage()));
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => new HomePageScreen()));
    }
  }

  @override
  void initState() {
    super.initState();
    new Timer(new Duration(seconds: 2), () {
      check();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            height: 80.0,
            width: 80.0,
            margin: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                image: AssetImage("assets/logo.png"),
                fit: BoxFit.cover
              )
            ),
          ),
      ),
    );
  }
}