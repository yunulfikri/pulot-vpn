import 'package:flutter/material.dart';


class ErrorScreen extends StatefulWidget {
  const ErrorScreen({Key key}) : super(key: key);

  @override
  _ErrorScreenState createState() => _ErrorScreenState();
}

class _ErrorScreenState extends State<ErrorScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          children: [
            Text("Something Wrong This App", style: TextStyle(fontWeight: FontWeight.bold),),
            Text("Please Update")
          ],
        ),
      ),
    );
  }
}
