

import 'package:flutter/material.dart';
import '../utils/ad_helper.dart';

import 'package:google_mobile_ads/google_mobile_ads.dart';
class HelpScreen extends StatefulWidget {
  @override
  _HelpScreenState createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  InterstitialAd _interstitialAd;
  bool _isInterstitialAdReady = false;
  @override
  void initState() {
    super.initState();
    _createInterstitialAd();
    
  }
  void _createInterstitialAd(){
    InterstitialAd.load(
      adUnitId: AdHelper.interstitialAdUnitId, 
      request: AdRequest(), 
      adLoadCallback: InterstitialAdLoadCallback(
        onAdLoaded: (InterstitialAd ad){
          _interstitialAd = ad;
        },
        onAdFailedToLoad: (LoadAdError error){
          print('InterstitialAd failed to load: $error.');
          _interstitialAd = null;
        }
      )
    );
  }
  void showInterstitialAd(){
    if (_interstitialAd == null) {
      print('Warning: attempt to show interstitial before loaded.');
      return;
    }
    _interstitialAd.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (InterstitialAd ad) =>
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (InterstitialAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        _createInterstitialAd();
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        _createInterstitialAd();
      },
    );
    _interstitialAd.show();
    _interstitialAd = null;
  }
  @override
  void dispose() {
    _interstitialAd.dispose();
    super.dispose();
  }

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Help"),
      ),
      body: SafeArea(child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                       color: Colors.grey.withOpacity(0.1),
                       spreadRadius: 2,
                        blurRadius: 4,
                        offset: Offset(4, 9), 
                    )
                  ]

                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Server List", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black)),
                    Text("At the moment we only have 1 server in Singapore", style: TextStyle(color: Colors.black)),
                    SizedBox(height: 5.0,),
                    Text("Premium Server", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black)),
                    Text("All of these are premium servers and these are all free for you", style: TextStyle(color: Colors.black)),
                    SizedBox(height: 5.0,),
                    Text("Ads", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black)),
                    Text("Sorry for the inconvenience due to advertising, advertising serves as our main revenue at this time", style: TextStyle(color: Colors.black)),
                    SizedBox(height: 5.0,),
                  ],
                ),
              ),

              SizedBox(height: 50.0,),
              Text("pulotvpn@gmail.com"),
              Text("Aceh, Indonesia"),
              Text("By ruangbawah.com"),
              Text("version: 1.0.5"),
              Divider(),
              Text("Support the improvement of this application by pressing the 'donate' button below."),
              MaterialButton(
                onPressed: (){
                  showInterstitialAd();
                },
                color: Color(0xAAFFE2A8),
                textColor: Color(0xAA1D3253),
                elevation: 5.0,
                minWidth: 120.0,
                child: Text("Donate", style: TextStyle(
                  fontWeight: FontWeight.bold
                ))
              ),
              Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Text("This button will bring up an ad, and this is of little help to us. Thank you", style: TextStyle(
                    fontWeight: FontWeight.w100,
                    fontSize: 11.0,
                    
                  )),
              )
            ],
          ),
        ),
      )),
    );
  }
}