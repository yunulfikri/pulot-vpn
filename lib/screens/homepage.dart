import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:developer';
import 'package:flutter/services.dart' show rootBundle;
import 'package:avatar_glow/avatar_glow.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:pulotvpn/screens/help.dart';
import 'package:pulotvpn/screens/prepare.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/engine.dart';
import '../core/models.dart';
import '../utils/ad_helper.dart';
class HomePageScreen extends StatefulWidget {
  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  String _vpnState = PulotVpn.vpnDisconnected;
  List<VpnConfig> _listvpn = [];
  VpnConfig _selectedVpn;

  BannerAd _bannerAd;
  bool _isBannerAdReady = false;

  @override
  void initState() {
    initServer();
    PulotVpn.vpnStageSnapshot().listen((event) {
      setState(() {
        _vpnState = event;
      });
    });
    _bannerAd = BannerAd(
      size: AdSize.banner, 
      adUnitId: AdHelper.bannerAdUnitId,
      request: AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (_){
          setState(() {
            _isBannerAdReady = true;
          });
        },
        onAdFailedToLoad: (ad, err) {
          print('Failed to load a banner ad: ${err.message}');
          _isBannerAdReady = false;
          ad.dispose();
        },
      ));

      _bannerAd.load();

      super.initState();
  }
  @override
  void dispose() {
    _bannerAd.dispose();
    super.dispose();
  }

  void initServer()async{
    SharedPreferences _sharedPreference = await SharedPreferences.getInstance();

    bool checkInit = _sharedPreference.getBool("ready");
    if(checkInit == false || checkInit == null){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => PrepareScreen())
      );
    }else {
      print("ready to connect");
     
    }
    initVpn();
  }

  void initVpn() async {
    print("vpn intitalize");
    SharedPreferences _sharedPreference = await SharedPreferences.getInstance();
    String configFile = _sharedPreference.getString('serverfile').toString();
    _listvpn.add(VpnConfig(
        name: "Singapore",
        flag: "assets/singapore.png",
        config: await rootBundle.loadString("assets/vpn/sg/"+configFile+".ovpn")));
    if (mounted)
      setState(() {
        _selectedVpn = _listvpn.first;
      });
  }

  void connectClick() {
    ///Stop right here if user not select a vpn
    if (_selectedVpn == null) return;

    if (_vpnState == PulotVpn.vpnDisconnected) {
      ///Start if stage is disconnected
      PulotVpn.startVpn(
        _selectedVpn,
        dns: DnsConfig("1.1.1.1", "1.0.0.1"),
      );
    } else {
      ///Stop if stage is "not" disconnected
      PulotVpn.stopVpn();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: AppBar(
          leading: Container(
            height: 20.0,
            width: 20.0,
            margin: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),

              image: DecorationImage(
                image: AssetImage("assets/logo.png"),
                fit: BoxFit.cover
              )
            ),
          ),
          title: Text(
            "Pulot VPN",
            style: Theme.of(context)
                .textTheme
                .headline6
                .copyWith(fontWeight: FontWeight.w600, fontSize: 24.0, fontFamily: GoogleFonts.dmSerifText().fontFamily),
          ),
          elevation: 0.0,
          centerTitle: true,
          actions: [
            IconButton(icon: Icon(Icons.help), onPressed: (){
              Navigator.push(context, 
                MaterialPageRoute(builder: (context) => HelpScreen())
              );
            })
          ],
        ),
        body: SafeArea(
          child: Stack(
            children: [
              Positioned(
                  top: 50,
                  child: Opacity(
                      opacity: 0.6,
                      child: Image.asset(
                        'assets/background.png',
                        fit: BoxFit.fill,
                        height: MediaQuery.of(context).size.height / 1.5,
                      ))),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),

                  child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 25),
                      Center(
                          child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: _vpnState == PulotVpn.vpnDisconnected
                              ? Colors.red
                              : Color.fromRGBO(37, 112, 252, 1),
                        ),
                        child: Text(
                          _vpnState == PulotVpn.vpnDisconnected
                              ? "DISCONNECTED"
                              : _vpnState.replaceAll("_", " ").toUpperCase(),
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                                color: Colors.white,
                              ),
                        ),
                      )),
                      SizedBox(height: 15),
                      Center(
                        child: AvatarGlow(
                          glowColor: _vpnState == PulotVpn.vpnDisconnected ? Colors.grey : Color.fromRGBO(37, 112, 252, 1),
                          endRadius: 120.0,
                          repeat: true,
                          duration: Duration(milliseconds: 2000),
                          repeatPauseDuration: Duration(milliseconds: 100),
                          showTwoGlows: true,
                          shape: BoxShape.circle,
                          child: InkWell(
                            onTap: connectClick,
                            child: Material(
                              elevation: 2,
                              shape: CircleBorder(),
                              color: _vpnState == PulotVpn.vpnDisconnected
                                  ? Colors.grey
                                  : Color.fromRGBO(37, 112, 252, 1),
                              child: SizedBox(
                                height: 150,
                                width: 150,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.power_settings_new,
                                      color: Colors.white,
                                      size: 50,
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      _vpnState == PulotVpn.vpnDisconnected
                                          ? "Connect"
                                          : "Disconnect",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .copyWith(color: Colors.white),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: StreamBuilder<VpnStatus>(
                          initialData: VpnStatus(),
                          stream: PulotVpn.vpnStatusSnapshot(),
                          builder: (context, snapshot){
                            return Text(
                              "${snapshot?.data?.byteIn ?? ""}, ${snapshot?.data?.byteOut ?? ""}",
                              textAlign: TextAlign.center);
                          },
                        ),
                      ),
                      SizedBox(height: 30.0,),
                      Text(
                        "Free Servers",
                        style: Theme.of(context).textTheme.headline6.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 8.0,),
                    ]
                    ..addAll(
                      _listvpn.length > 0
                        ? _listvpn.map(
                            (e) => ListTile(
                              title: _selectedVpn == e
                                        ? Text(e.name, style: TextStyle(color: Color.fromRGBO(37, 112, 252, 1))) 
                                        : Text(e.name, style: TextStyle(color: Colors.grey)),
                              leading: SizedBox(
                                height: 50,
                                width: 50,
                                child: Center(
                                  child:  CircleAvatar(
                                    backgroundImage: AssetImage(e.flag),
                                    backgroundColor: Colors.green)
                                  )
                              ),
                              onTap: () {
                                if (_selectedVpn == e) return;
                                log("${e.name} is selected");
                                PulotVpn.stopVpn();
                                setState(() {
                                  _selectedVpn = e;
                                });
                              },
                            ),
                          )
                        : [],
                  ),
                  ),
                ),
              ),
              if (_isBannerAdReady) Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: _bannerAd.size.width.toDouble(),
                  height: _bannerAd.size.height.toDouble(),
                  child: AdWidget(ad: _bannerAd),
                ),
              )
            ],
          ),
        ));
  }
}