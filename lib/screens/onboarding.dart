import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:pulotvpn/screens/prepare.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';

class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  FirebaseAuth auth = FirebaseAuth.instance;
  UserCredential userCredential;
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("acceptterms", "true");
    userCredential = await FirebaseAuth.instance.signInAnonymously();

    print(userCredential.user.uid.toString());
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_) => PrepareScreen()),
    );
  }

  // Widget _buildFullscrenImage() {
  //   return Image.asset(
  //     'assets/background.png',
  //     fit: BoxFit.cover,
  //     height: double.infinity,
  //     width: double.infinity,
  //     alignment: Alignment.center,
  //   );
  // }

  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset('assets/$assetName', width: width);
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0, color: Colors.black87);

    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );
    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: Colors.white,
      globalHeader: Align(
        alignment: Alignment.topRight,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 16, right: 16),
            child: _buildImage('flutter.png', 100),
          ),
        ),
      ),
      globalFooter: SizedBox(
        width: double.infinity,
        height: 60,
        child: ElevatedButton(
          child: const Text(
            'Let\s go right away!',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          onPressed: () => _onIntroEnd(context),
        ),
      ),
      pages: [
        PageViewModel(
          title: "Secure and Private",
          body:
              "A VPN service provides you a secure, encrypted tunnel for online traffic to flow. ",
          image: _buildImage('undraw_SecurityOn.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Protect your devices",
          body:
              "Securely access personal information or work files, encrypt your internet connection, and keep your browsing history private.",
          image: _buildImage('undraw_encryption.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "No longger watching you",
          body:
              "Nobody can see through the tunnel and get their hands on your internet data.",
          image: _buildImage('undraw_Surveillance.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Our Commitment to Privacy",
          body:
              "By using this app, you agree to our Privacy Policy and Terms of Service",
          image: _buildImage('undraw_terms.png'),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      //rtl: true, // Display as right-to-left
      skip: const Text('Skip'),
      next: const Icon(Icons.arrow_forward),
      done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600)),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: kIsWeb
          ? const EdgeInsets.all(12.0)
          : const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        color: Colors.black87,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}